import "./login.styles.css";
import { component$, $ } from "@builder.io/qwik";

export const LoginForm = component$(() => {


  const onFetch = $(async () => {
    const api = new URL("http://127.0.0.1:6969/api/sign-in");

    const myHeaders: Headers = new Headers();
    myHeaders.append("Content-Type", "application/json");

    const raw = JSON.stringify({
      email: "test@email.com",
      password: "test@1234",
    });

    const requestOptions: RequestInit = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };

    await fetch(api, requestOptions)
      .then((response) => response.text())
      .then((result) => console.log(result))
      .catch((error) => console.log("error", error));

    console.log(api);
  });

  return (
    <form>
      <h1>Sign In Form</h1>
      <div class="input-item">
        <label for="email">Email</label>
        <input
          type="email"
          name="email"
          id="email"
          placeholder="Enter Your Email id . . ."
          // onInput$={(e: Event) => props.email = e.target?.value}
        />
      </div>
      <div class="input-item">
        <label for="password">Password</label>
        <input
          type="password"
          name="password"
          id="password"
          placeholder="Enter Your Password. . ."
        />
      </div>
      <button preventdefault:click type="submit" onClick$={onFetch}>
        Login
      </button>
    </form>
  );
});
