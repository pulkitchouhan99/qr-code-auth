const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const qrsvg = require("./src/qr-svg-generator");
const app = express();
const port = process.env.PORT || 6969;
const validate = require("./src/validator");
const tokenGenerate = require("./src/tokenGen")

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());

app.post("/api/sign-in", async (req, res) => {
  try {
    const user = { email: req.body.email, password: req.body.password };
    const validatedUser = await validate(user);
    let svg;
    if (validatedUser!==null) {
      const token = await tokenGenerate(validatedUser)   
      const authURL = `${req.protocol}://${req.hostname}:${port}/api/auth-check/${token.uuid}`;
      console.log("auth :",authURL)
      svg = qrsvg(authURL);
      if (svg) {
        return res.send(svg);
      }else{
        return res.status(400).send({
          message: "Bad Request!"
        })
      }
    } else {
      return res.status(400).send({
        message: "User not Found!",
      });
    }    
  } catch (err) {
    console.error(err);
    return res.status(500).send({
      message: "This is an error!",
    });
  }
});

app.get("/api/auth-check/:id", async (req, res) => {
  try {
    console.log(req.params.id);
    return res.send("Authenticated!");
  } catch (err) {
    return res.status(400).send({
      message: "This is an error!",
    });
  }
});

app.listen(port, () => {  
  console.log(`Example app listening on port ${port}`);
});
